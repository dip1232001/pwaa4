import { Component, OnInit } from '@angular/core';
import { StartupService } from './startup.service';
// tslint:disable-next-line:quotemark
import { Router } from "@angular/router";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {


  constructor(private router: Router, private startup: StartupService) {

  }

  ngOnInit() {
    if (!this.startup.startupData) {
      this.router.navigate(['notfound'], { replaceUrl: false });
    }
  }
}
