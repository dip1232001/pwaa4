import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import { Constants } from './constants';
import { ActivatedRoute } from '@angular/router';


@Injectable()
export class StartupService {

    private _startupData: any;
    private buid: string;
    constructor(private http: Http) { }

    // This is the method you want to call at bootstrap
    // Important: It should return a Promise
    load(): Promise<any> {
        Constants.BUID = this.buid = window.location.pathname;
        this._startupData = null;

        console.log(Constants.CallManiFest + this.buid);

        return this.http
            .get(Constants.CallManiFest + this.buid + '/manifest.json')
            .map((res: Response) => res.json())
            .toPromise()
            .then((data: any) => this._startupData = data)
            .catch((err: any) => Promise.resolve());
    }

    get startupData(): any {
        return this._startupData;
    }
}
