import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})



export class HeaderComponent implements OnInit {


  constructor(meta: Meta, title: Title) {
    title.setTitle("Pwa");
    meta.addTags([
      {
        content: 'Great Device_1',
        name: 'description'
      },
      {
        content: 'yes',
        name: 'mobile-web-app-capable'
      }
    ]);
  }

  ngOnInit() {
    jQuery(function ($) {
      $('.sidenavopenar').sideNav({
        closeOnClick: true,
        draggable: false
      });
    });
  }

}
