import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultComponent } from './default/default.component';
import { WebsitesComponent } from './websites/websites.component';
import { HeaderComponent } from './header/header.component';
import { StartupService } from './startup.service';
import { HttpModule } from '@angular/http';

export function startupServiceFactory(startupService: StartupService): Function {
  return () => startupService.load();
}
@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    WebsitesComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    StartupService,
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
