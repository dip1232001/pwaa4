import { Pwaa4Page } from './app.po';

describe('pwaa4 App', () => {
  let page: Pwaa4Page;

  beforeEach(() => {
    page = new Pwaa4Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
